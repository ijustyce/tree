create table if not exists tree
(
    nodeId    varchar(32)  not null primary key comment '节点Id',
    refId     varchar(32)  not null comment '关联的Id',
    parentId  varchar(32)  default null comment '父节点Id',
    lft   integer not null comment '左值',
    rgt    integer not null comment '右值',
    deep    integer   not null comment '树的深度',
    treeType tinyint not null comment '树的类型',
    index parentIndex (parentId),
    index refIdIndex (refId),
    createdAt datetime     not null default current_timestamp,
    updateAt  datetime     not null default current_timestamp
) charset = utf8mb4
  collate = utf8mb4_unicode_ci;