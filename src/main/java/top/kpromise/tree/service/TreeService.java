package top.kpromise.tree.service;

import top.kpromise.tree.model.sql.Tree;

public interface TreeService {
    
    int insert(Tree tree);
    int update(Tree tree);
    Tree selectByNodeId(String nodeId);
    int deleteByNodeId(String nodeId);
    int updateLftValue(int value, int begin, int treeType);
    int updateRgtValue(int value, int begin, int treeType);
    int childCount(int lft, int rgt);
}
