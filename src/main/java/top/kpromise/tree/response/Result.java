package top.kpromise.tree.response;

import java.util.HashMap;

public class Result {

    private static HashMap<String, Object> data(Object data, String message, int code) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("message", message);
        result.put("code", code);
        if (data != null) {
            result.put("data", data);
        }
        return result;
    }

    public static HashMap<String, Object> data(Object data, String message) {
        return data(data, message, 100);
    }

    public static HashMap<String, Object> data(String message) {
        return data(null, message);
    }

    public static HashMap<String, Object> error(String message) {
        return data(null, message, 101);
    }

    public static HashMap<String, Object> data(Object data) {
        return data(data, "请求成功");
    }
}
