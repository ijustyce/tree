package top.kpromise.tree.mapper.generated;

import org.springframework.stereotype.Repository;
import top.kpromise.tree.model.sql.Tree;

@Repository
public interface TreeMapper {
    int deleteByPrimaryKey(String nodeId);

    int insert(Tree record);

    int insertSelective(Tree record);

    Tree selectByPrimaryKey(String nodeId);

    int updateByPrimaryKeySelective(Tree record);

    int updateByPrimaryKey(Tree record);
}