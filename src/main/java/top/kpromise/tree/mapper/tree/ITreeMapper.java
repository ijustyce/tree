package top.kpromise.tree.mapper.tree;

import org.springframework.stereotype.Repository;

@Repository
public interface ITreeMapper {

    int updateLftValue(int value, int begin, int treeType);
    int updateRgtValue(int value, int begin, int treeType);
    int childCount(int lft, int rgt);
}
