package top.kpromise.tree.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static Date currentDate(Date date) {
        if (date == null) date = new Date();
        return parseDate(date, "yyyy-MM-dd hh:mm:ss");
    }

    public static Date currentDate() {
        return currentDate(new Date());
    }

    public static String formatDate(Date date, String format) {
        if (date == null) return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.CHINA);
        return simpleDateFormat.format(date);
    }

    public static Date parseDate(Date date, String format) {
        if (date == null) return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.CHINA);
        return parseDate(simpleDateFormat.format(date), format);
    }

    public static Date parseDate(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.CHINA);
        try {
            return simpleDateFormat.parse(date);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date parseBirthday(String date) {
        return parseDate(date, "yyyy-MM-dd");
    }

    public static String formatBirthday(String date) {
        return formatDate(parseBirthday(date), "yyyy-MM-dd");
    }

    public static String formatBirthday(Date date) {
        return formatDate(date, "yyyy-MM-dd");
    }
}
