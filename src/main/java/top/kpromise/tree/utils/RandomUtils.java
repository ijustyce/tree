package top.kpromise.tree.utils;

import java.util.Random;

public class RandomUtils {

//    public static String userId() {
//        /**
//         * 共有 58 个字母，58中选16 允许重复，将是 58^16 = 1.64*10^28 = 1.64*10^13 千万亿
//         * 假设您需要 千万亿的数据（或者已经有千万亿的数据），那么后面的数据重复的概率是 1.64*10^13 分之一，
//         * 即 16.4 万亿分之一
//         */
//        String letters = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
//        return RandomStringUtils.random(16, letters);
//    }

    public static String userId() {
        Long userId = SnowflakeIdWorker.generateLong();
        Hashids hashids = new Hashids();
        return hashids.encode(userId);
    }

    public static String ticket() {
        Long userId = SnowflakeIdWorker.generateLong();
        Hashids hashids = new Hashids();
        return hashids.encode(userId);
    }

    public static String verifyCode(int len) {
        StringBuilder builder = new StringBuilder();
        Random random = new Random();
        for (int index = 0; index < len; index++) {
            builder.append(random.nextInt(10));
        }
        return builder.toString();
    }
}