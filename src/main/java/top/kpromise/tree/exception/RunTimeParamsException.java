package top.kpromise.tree.exception;

public class RunTimeParamsException extends RuntimeException {

    private int code;

    public RunTimeParamsException(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
