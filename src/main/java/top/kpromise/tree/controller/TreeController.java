package top.kpromise.tree.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.kpromise.tree.model.sql.Tree;
import top.kpromise.tree.response.Result;
import top.kpromise.tree.service.TreeService;

import java.util.Map;

@RestController
@RequestMapping("/tree")
public class TreeController {

    private final TreeService treeService;

    public TreeController(TreeService treeService) {
        this.treeService = treeService;
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    public Map<String, Object> add(@RequestBody Tree tree) {
        treeService.insert(tree);
        return Result.data("创建成功");
    }
}
