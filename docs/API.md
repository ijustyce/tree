## 接口文档

### 创建节点
 - PUT /tree/add  
 ```json
 {
    "refId": "xxx",
    "parentId": "xxx",
    "treeType": 1
 }
 ``` 
 refId 关联的Id，可以是用户Id 等, parentId 父节点Id，可为空， treeType 树的类型